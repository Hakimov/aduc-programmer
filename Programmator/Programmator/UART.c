/*
 * UART.c
 *
 * Created: 16.08.2017 8:04:17
 *  Author: hakimov_a
 */ 


#include "UART.h"

/********************************************************************************/
// UART

//-----------------------------------------------------------------------------
void USART_Init( unsigned int baud )
{
	// ������������� 
	
	int ubrr	= ( F_CPU / 16 / baud - 1 );
	/*Set baud rate */
	UBRR0H		= ( unsigned char ) ( ubrr >> 8 );
	UBRR0L		= ( unsigned char ) ubrr;
	/* Enable receiver and transmitter */
	UCSR0B		= ( 1 << RXEN0 ) | ( 1 << TXEN0 );
	/* Set frame format: 8data, 2stop bit */
	UCSR0C		= ( 0 << USBS0 ) | ( 3 << UCSZ00 ) 
				| ( 0 << UPM01 )  | ( 0 << UPM00 ); // �������� ( 00 - ��������, 01 - ������, 10 - �������� ��������, 11 - �������� ���������� )
	RECEIVE_MODE;
}

//-----------------------------------------------------------------------------
void USART_Parity( bool state )
{	
	// ���������� ���������
	// ���������: PARITY_NONE - �������� ���������, PARITY_EVEN - �������� �������� 
	
	UCSR0C = ( 0 << USBS0 ) | ( 3 << UCSZ00 ) | ( state << UPM01 ) | ( 0 << UPM00 ); // �������� ( 00 - ��������, 01 - ������, 10 - �������� ��������, 11 - �������� ���������� )
}

//-----------------------------------------------------------------------------
void USART_Transmit( uint8_t byte )
{		
	// ������� ��� �������� ������ ����� ������ �� UART
	// ���������� �������� ���������� ��-�� ��������������� UART - RS-485	
	
	TRANSMIT_MODE;  // <---- ���� ���� ����� ����� ��� ��� ��� ��������
	_delay_us( 1000 );   // <--------------------------/	(������� �� ���� ��� ������ �������� ��������)
	while ( !( UCSR0A & ( 1 << UDRE0 ) ) )  //        /
		;									//       /
	UDR0 = byte;                            //      /
	_delay_us( 1000 );  // <-----------------------/
}


//-----------------------------------------------------------------------------
uint32_t hcount = 0;
uint32_t lcount = 0;
uint8_t USART_Receive( void )
{	
	// ������� ��� ������ ������ �� UART, ��� ���������� ��������� ��� ������� ��� ������ 
	// �� ������� ����� ��� �������� ������ ������
	
	RECEIVE_MODE;
	_delay_ms( 1 );
	/* Wait for data to be received */
	while ( !( UCSR0A & ( 1 << RXC0 ) ) )
	{		
		++lcount;
		if( lcount > 0xFFFE )
 			++hcount;
 		if( hcount > 0xFFFE )
			return 0xFF; 
	}
	return UDR0;
}


//-----------------------------------------------------------------------------
uint8_t _USART_Receive( void )
{	
	
	RECEIVE_MODE;
	_delay_ms( 1 );
	/* Wait for data to be received */
	while ( !( UCSR0A & ( 1 << RXC0 ) ) );
	return UDR0;
}

//-----------------------------------------------------------------------------
void SendStr( char *str )
{
	// ������� ��� �������� ������ �� UART, ������������ ��� ������� � �������� printf() ������ � ������������ ���������
	
	while( str[ 0 ] != 0 )
	{
		USART_Transmit( str[ 0 ] );
		*str++;
	}		
}

//-----------------------------------------------------------------------------
void SendArray( uint8_t *array, uint32_t length )
{
	// ������� ��� �������� ������� �� UART
	
	for( int i = 0; i < length; ++i )
		USART_Transmit( array[ i ] );
}


