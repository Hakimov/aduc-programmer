/*
 * modbus.c
 *
 * Created: 28.08.2017 15:58:01
 *  Author: hakimov_a
 */ 


#include "modbus.h"

//-----------------------------------------------------------------------------
// Compute the MODBUS RTU CRC
// ������� ��� ������� ����������� ����� ��������� MODBUS (������ �� ���������)
static uint16_t ModRTU_CRC( uint8_t *buf, int len )
{
	uint16_t crc = 0xFFFF;
	for ( int pos = 0; pos < len; pos++ ) 
	{
		crc ^= ( uint16_t ) buf[ pos ];		// XOR byte into least sig. byte of crc
		for ( int i = 8; i != 0; i-- ) 
		{									// Loop over each bit
			if( ( crc & 0x0001 ) != 0 ) 
			{								// If the LSB is set
				crc >>= 1;					// Shift right and XOR 0xA001
				crc ^= 0xA001;
			}
			else							// Else LSB is not set
				crc >>= 1;					// Just shift right
		}
	}
	// Note, this number has low and high bytes swapped, so use it accordingly (or swap bytes)
	return crc;  
}

//-------------------------------------
// ������ ������������� �� ���-������
// $F7$2B$0E$01$00$B8$62	// ���. �����
// $77$2B$0E$01$00$B9$BC	// ����. �����
//-------------------------------------

//-----------------------------------------------------------------------------
// ������� �������� ������� ������ ��������, ���������� ������ ID ������(��������� ��� �����������) 
static void Request_Send( uint8_t DEVICE_ID )
{
	uint8_t buf[ 7 ];
	buf[ 0 ] = DEVICE_ID;
	buf[ 1 ] = Function_code;
	buf[ 2 ] = MEI_Type;
	buf[ 3 ] = Read_Device_ID_code;
	buf[ 4 ] = Next_Object_Id;	
	uint16_t CRC = ModRTU_CRC( buf, 5 );	
	buf[ 5 ] = CRC; 
	buf[ 6 ] = CRC >> 8;	
	SendArray( &buf, 7 );
}

/* ��� ������ ������ ������� � DKU_Reader
000001 14:29:52.175  COM5   << 77 2B 0E 01 00 B9 BC                              w+...??
000002 14:29:52.306  COM5   >> 77 2B 0E 01 01 00 00 03 00 09 22 4E 50 43 50 52   w+........"NPCPR
                               4F 4D 22 01 08 22 44 4B 55 2D 30 33 22 02 05 76   OM".."DKU-03"..v
                               34 5F 72 32 82 F9                                 4_r2�?
							   
000005 14:29:52.306  COM5   << F7 2B 0E 01 00 B8 62                              ?+...?b
000006 14:29:52.436  COM5   >> F7 2B 0E 01 01 00 00 03 00 09 22 4E 50 43 50 52   ?+........"NPCPR
                               4F 4D 22 01 08 22 44 4B 55 2D 30 33 22 02 05 76   OM".."DKU-03"..v
                               34 5F 72 32 94 99                                 4_r2��
*/
 // ��� ����� �� ���, ���� ��� ����� �������� � �������� � ���������� ����� ������
 // $F7$2B$0E$01$01$00$00$03$00$09$22$4E$50$43$50$52$4F$4D$22$01$08$22$44$4B$55$2D$30$33$22$02$05$76$34$5F$72$32$94$99

//-----------------------------------------------------------------------------
// ������� ��� ������� ������ ��������
void request_sofware_version( uint8_t DEVICE_ID )
{
	char main_numb_str[ 4 ] = "119:";
	char dubl_numb_str[ 4 ] = "247:";
	uint8_t line = 0;
	uint8_t buf[ 100 ];
	memset( &buf, 0, 100 );
	ERROR_CODES = NoError;
	
	USART_Parity( PARITY_EVEN );
	_delay_ms( 100 );
	// ��� ������ ������ ������� ����������� � ������ ������ ��������� ������
	if( DEVICE_ID == MODBUS_DEVICE_ID0 )	// ���� ��������
	{
		line = 2;
		SetString( line, 0, main_numb_str, sizeof( main_numb_str ) );	// �� ������� ������ �� ������ ���� �������
	}			
	if( DEVICE_ID == MODBUS_DEVICE_ID1 )		// ���� �����������
	{
		line = 0;
		SetString( line, 0, dubl_numb_str, sizeof( dubl_numb_str ) );	// �� �� ��������� ���� �������
	}		
	
	Request_Send( DEVICE_ID );					// �������� ������
	//-------------------------------------
	while( buf[ 0 ] != DEVICE_ID )
		buf[ 0 ] = USART_Receive();				// �������� ��������� �����,
		
	if( buf[ 0 ] != DEVICE_ID )					// �������������
	{											// � �������� ������ � ������ �� ����������
		SetString( line, sizeof( main_numb_str ), "DEV_ID Err", 10 ); 
		ERROR_CODES = DEV_ID_Err;
		_delay_ms( 100 );
	}		
	//-------------------------------------
	buf[ 1 ] = USART_Receive();
	if( buf[ 1 ] != Function_code )
	{
		SetString( line, sizeof( main_numb_str ), "FuncCode Err", 12 );
		ERROR_CODES = FuncCode_Err;
		_delay_ms( 100 );
	}		
	//-------------------------------------
	buf[ 2 ] = USART_Receive();
	if( buf[ 2 ] != MEI_Type )
	{
		SetString( line, sizeof( main_numb_str ), "MEI_Type Err", 12 );
		ERROR_CODES = MEI_Type_Err;
		_delay_ms( 100 );
	}		
	//-------------------------------------
	buf[ 3 ] = USART_Receive();
	if( buf[ 3 ] != Read_Device_ID_code )
	{
		SetString( line, sizeof( main_numb_str ), "DevIDcode Err", 12 );
		ERROR_CODES = DevIDcode_Err;
		_delay_ms( 100 );
	}		
	//-------------------------------------
	buf[ 4 ] = USART_Receive();
	if( buf[ 4 ] != Conformity_level )
	{
		SetString( line, sizeof( main_numb_str ), "Conf_lvl Err", 12 );
		ERROR_CODES = Conf_lvl_Err;
		_delay_ms( 100 );
	}		
	//-------------------------------------
	buf[ 5 ] = USART_Receive();
	if( buf[ 5 ] != More_Follows )
	{
		SetString( line, sizeof( main_numb_str ), "MoreFllws Err", 13 );
		ERROR_CODES = MoreFllws_Err;
		_delay_ms( 100 );
	}		
	//-------------------------------------
	buf[ 6 ] = USART_Receive();
	if( buf[ 6 ] != Next_Object_Id )
	{
		SetString( line, sizeof( main_numb_str ), "NextObjId Err", 13 );
		ERROR_CODES = NextObjId_Err;
		_delay_ms( 100 );
	}
	//-------------------------------------
	buf[ 7 ] = USART_Receive();		// ����� ��������� ���������� ��������
	uint8_t number_of_objects = buf[ 7 ];
	//-------------------------------------
	uint8_t count_index = 8;	
	uint8_t object_ID = 0;
	uint8_t object_length = 0;
	uint8_t k = 1;
	if( number_of_objects < 80 )
	{
		uint8_t object_buf[ number_of_objects ][ 20 ];
		do 
		{
			buf[ count_index ] = USART_Receive();
			object_ID = buf[ count_index ];
			++count_index;
			buf[ count_index ] = USART_Receive();
			object_length = buf[ count_index ];
			object_buf[ number_of_objects - 1 ][ 0 ] = buf[ count_index ];		
			++count_index;
			for( k = 1; k <= object_length; ++k )
			{
				buf[ count_index ] = USART_Receive();
				object_buf[ number_of_objects - 1 ][ k ] = buf[ count_index ];
				++count_index;
			}
			--number_of_objects;
		} while( number_of_objects > 0x00 );
	
		buf[ count_index ] = USART_Receive();
		++count_index;
		buf[ count_index ] = USART_Receive();
		++count_index;
		/*
		if( ERROR_CODES != NoError )
			return;
			*/
		// ����������� ����������� �����
		uint16_t	CRC			= ModRTU_CRC( buf, count_index - 2 );
		uint8_t		tempCRC_L	= CRC >> 8;
		uint8_t		tempCRC_H	= CRC;	
		// � ���������� ����������� �������� � ��������, ���� ������� �� ������� ������ � ���������� ���������� ���������
		if( ( buf[ count_index - 1 ] != tempCRC_L ) || 
			( buf[ count_index - 2 ] != tempCRC_H ) )
		{
			SetString( line, sizeof( main_numb_str ), "CRC Error", 9 );
			_delay_ms( 2000 );
			return;
		}
		USART_Parity( PARITY_NONE );
		
		// ����� ������ �������� ��������� � ������������ ������ �� �������
		SetString(	line, 
					sizeof( main_numb_str ), 
					&object_buf[ 2 ][ 1 ], 
					object_buf[ 2 ][ 0 ] );	
					
		SetString(	line + 1, 
					0, 
					&object_buf[ 1 ][ 1 ], 
					object_buf[ 1 ][ 0 ] );		
							
		SetString(	line + 1, 
					object_buf[ 1 ][ 0 ], 
					&object_buf[ 0 ][ 1 ], 
					object_buf[ 0 ][ 0 ] );	
	}
		
/* ����� ����� �������� �����
 ___________________________
|      ----------------     |
|    0|119:"NPCPROM"   |    |
|    1|"DKU-03"v4_r2   |    |
|    2|247:"NPCPROM"   |    |
|    3|"DKU-03"v4_r2   |    |
|      ----------------     |
|      0123456789012345     |
|___________________________|


���� ����� ������
 ___________________________
|      ----------------     |
|    0|119:Error       |    |
|    1|                |    |
|    2|247:CRC Error   |    |
|    3|                |    |
|      ----------------     |
|      0123456789012345     |
|___________________________|
 */
}
