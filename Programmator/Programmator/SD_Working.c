/*
 * SD_Working.c
 *
 * Created: 24.08.2017 13:10:40
 *  Author: hakimov_a
 */ 

#include "SD_Working.h"

FATFS fs; //��������� �� ������
FRESULT res; //��������� ����������
WORD s1;
DIR dir;
FILINFO fileinfo;

// ��������� ������ �������� �����, ���� ���������� hex � ���������� � �����
typedef struct _PATH
{
	FILINFO f_inf;
	char folder_file[ 16 ];
	bool HEX;	
} _path;

const char hexstr[ 4 ] = ".HEX";
_path custom_path[ 32 ];	// ������� ������ ��� �������� ��������� ������ ( �����, ����� )
volatile int8_t index_count = 0;
volatile int8_t list_count = 0;
volatile uint8_t path_index = 2;			// ���������� ������� ������ ����� ����� � ����������	

/********************************************************************************/
// SD


//----------------------------------------------------------------------------------------------------------------
static bool SD_Mount()
{
	// ������������ ������
	
	res = pf_mount( &fs );
    if( res == FR_OK )
		return true;
    else 
		return false;
}

//----------------------------------------------------------------------------------------------------------------
static bool SD_UNMount()
{
	// �������������� ������
	
	res = pf_mount( 0x00 );
    if( res == FR_OK )
		return true;
    else 
		return false;
}

//----------------------------------------------------------------------------------------------------------------
static bool SD_OpenFile( char *path )
{
	// �������� ����� ��� �����
	
	res = pf_open( path ); //������� ������� ����
    if( res == FR_OK ) 
		return true;
    else 
		return false;
}	
	
//----------------------------------------------------------------------------------------------------------------
static bool SD_ReadFile( uint16_t point_begin, uint16_t number, unsigned char buf[] )
{
	// ������ �����
	
	unsigned char temp[ number + 1 ];
	res = pf_lseek( point_begin ); //��������� ������ ������ �� 0
    if( res != FR_OK ) 
		return false;
	res = pf_read( &temp, number, &s1 );	
    if( res != FR_OK ) 
		return false;			
	memcpy( buf, &temp, number );
	return true;
}	

//----------------------------------------------------------------------------------------------------------------
static void root_dir_init()
{
	// ������������� ������� � ������� �������� �������� ����� �������� ����� "Back", ����� ��� ������� 
	// ����� �������� ������� ����� � � ������ �������� ����� �������� ���� � ����� "/"
	
	_path	temp_path;
	memset( &temp_path.f_inf.fname, 0, 13 );
	for( int8_t i = 1; i <= 31; ++i )
		custom_path[ i ] = temp_path;
	char back[ 5 ] = "Back";
	back[ 4 ] = 0;
	memcpy( &custom_path[ 0 ].f_inf.fname, &back, 4 );	 
	custom_path[ 1 ].f_inf.fname[ 0 ] = '/';	// ������� ������� ������ ����� ��������� ������ �� ������
	custom_path[ 1 ].f_inf.fname[ 1 ] = 0x00;	// ���������� ���� �� ������ ������� ������� 
	custom_path[ 1 ].HEX = false;				// ����� �� ������� ��� ������� ������ �� ����� ����������  *.hex
}

//----------------------------------------------------------------------------------------------------------------
static void root_dir_deinit()
{
	// 
	
	_path temp_path;
	memset( &temp_path.f_inf.fname, 0, 13 );
	for( int8_t i = 1; i <= 31; ++i )
		custom_path[ i ] = temp_path; 
	index_count = 0;
	list_count = 0;
	path_index = 2;
}

static char strr[ 15 ];
//----------------------------------------------------------------------------------------------------------------
static void Show_microcontroller_name_and_version()
{	
	// ����� �������� � ������ ���������������� �� �������
	
	memset( strr, 0, 15 );
	SetString( 0, 0, "Waiting for", 11 );
	SetString( 1, 0, "connection...", 13 );
	if( Request_of_microcontroller_version( strr ) )
	{
		Clear_Display();
		_delay_ms( 100 );
		SetString( 3, 0, &strr, 12 );
	}
	else
		Clear_Display();
	memset( strr, 0, 15 );
}

//----------------------------------------------------------------------------------------------------------------
static int8_t arrow_index_calculate( int8_t index_count )
{			
	// ��� ������� ���������� ��� ����������� ������ ���������(������� ">" ) ��� ������������ �� ����
	
	int tmp_numb = ( ( double ) index_count / ( double ) 4 ) * 100;
	if( tmp_numb % 100 == 0 )
		return 0;
	else if( tmp_numb % 100 == 25 )
			return 1;
		else if( tmp_numb % 100 == 50 )
				return 2;
			else if( tmp_numb % 100 == 75 )
					return 3;						
}

//----------------------------------------------------------------------------------------------------------------
static void Check_for_HEX_procedure( int8_t s )
{
	// ��� ��������� ��� ����������� ����� �� ���� ���������� *.hex
	if( custom_path[ s ].f_inf.fname[ strlen( custom_path[ s ].f_inf.fname ) - 1 ] == hexstr[ 3 ] )
		if( custom_path[ s ].f_inf.fname[ strlen( custom_path[ s ].f_inf.fname ) - 2 ] == hexstr[ 2 ] )
			if( custom_path[ s ].f_inf.fname[ strlen( custom_path[ s ].f_inf.fname ) - 3 ] == hexstr[ 1 ] )
				if( custom_path[ s ].f_inf.fname[ strlen( custom_path[ s ].f_inf.fname ) - 4 ] == hexstr[ 0 ] )
					custom_path[ s ].HEX = true;
}

//----------------------------------------------------------------------------------------------------------------
static uint8_t CRC_HEX( HEX_File* hex_File )
{
	// ������� ��� ������� ����������� ����� ������ HEX �����
	
	uint8_t dataSum = 0;	
	for ( int i = 0; i < hex_File->recordLength; ++i )
		dataSum += hex_File->Data[ i ];
	uint8_t result = 0x0100 - (		dataSum + hex_File->recordType + 
									hex_File->loadAdDressL + 
									hex_File->loadAdDressH + 
									hex_File->recordLength );	
	return result;
}

//----------------------------------------------------------------------------------------------------------------
static uint32_t HEX_BUF_Handler( char *HEX_BUF, int32_t size, uint32_t tail )
{
	// ������� ��������� ������ hex ����� � ������������ �������� � ���������� �� ��������� � ����������� ��������
	// �  ProgramblockofFlashProgramMemory( &hex ) ��� ���������� ������� � ADUC
	
	uint32_t	i			= 0;
	uint32_t	temp_i		= 0;
	uint16_t	hh = 0, ll	= 0;
	
	HEX_File	hex;
	char		numbuf[ 2 ];
		
	while( 1 )
	{
		tail += temp_i ;
		temp_i = 0;	
		
		while( HEX_BUF[ i ] != ':' )
		{
			++i; 
			++temp_i;
		}		
		
		hex.recordMark = HEX_BUF[ i ]; ++i; ++temp_i;
		
		numbuf[ 0 ] = HEX_BUF[ i ]; ++i; ++temp_i;
		numbuf[ 1 ] = HEX_BUF[ i ]; ++i; ++temp_i;
		hex.recordLength = strtol( &numbuf, NULL, 16 );
		
		numbuf[ 0 ] = HEX_BUF[ i ]; ++i; ++temp_i;
		numbuf[ 1 ] = HEX_BUF[ i ]; ++i; ++temp_i;
		hex.loadAdDressH = strtol( &numbuf, NULL, 16 );
		
		numbuf[ 0 ] = HEX_BUF[ i ]; ++i; ++temp_i;
		numbuf[ 1 ] = HEX_BUF[ i ]; ++i; ++temp_i;
		hex.loadAdDressL = strtol( &numbuf, NULL, 16 );
		
		numbuf[ 0 ] = HEX_BUF[ i ]; ++i; ++temp_i;
		numbuf[ 1 ] = HEX_BUF[ i ]; ++i; ++temp_i;
		hex.recordType = strtol( &numbuf, NULL, 16 );
		
		if( hex.recordLength == 0x00 )
			if ( hex.recordType == END_RECORD )
				return 0xFFFF;
				
		for( uint32_t iter = 0; iter < hex.recordLength; ++iter )
		{
			numbuf[ 0 ] = HEX_BUF[ i ]; ++i; ++temp_i;
			numbuf[ 1 ] = HEX_BUF[ i ]; ++i; ++temp_i;
			hex.Data[ iter ] = strtol( &numbuf, NULL, 16 );
		}	
		
		numbuf[ 0 ] = HEX_BUF[ i ]; ++i;  ++temp_i;
		numbuf[ 1 ] = HEX_BUF[ i ];	
		hex.Checksum = strtol( &numbuf, NULL, 16 );
		
		L_GREEN_RESET;
		L_RED_RESET;
		
		if( hex.Checksum != CRC_HEX( &hex ) )
			return tail;	
		else
		{
			if( ProgramblockofFlashProgramMemory( &hex ) == ResponseACK )
			{
				L_GREEN_SET;
				hh = tail / 1000;
				ll = tail % 1000 / 100;
				sprintf( &str, "%u,%u kB ", hh, ll );
				SetString( 2, 0, &str, strlen( &str ) );
			}				
			else
			{
				L_RED_SET;
				return 0;
			}
		}		
	}
}

//----------------------------------------------------------------------------------------------------------------
static bool HEX_Read( unsigned long fsize )
{
	// ������� ��� �������� HEX ����� � ���������� �������� � ������� ( ProgramblockofFlashProgramMemory ) 
	// ��� ����������� ������������ ������� � �� ������. ��� �� ����� ������������ ��������� ������ HEX �����
	
	const uint32_t BUF_size = 1024;
	unsigned char HEX_BUF[ BUF_size ];
	memset( &HEX_BUF, 0, BUF_size );
	
	uint32_t range =BUF_size - 1;
	uint32_t tail =	0;
	uint32_t head =	tail + range;
	
	uint32_t i = 0;
		
	while( tail != 0xFFFF )
	{
		res = pf_lseek( tail ); //��������� ������ ������ �� 0
		if( res != FR_OK ) 
			return false;
		res = pf_read( &HEX_BUF, (head - tail), &s1 );	
		if( res != FR_OK ) 
			return false;			
			
		tail = HEX_BUF_Handler( HEX_BUF, BUF_size, tail );
		head = tail + range;
	}
	return true;
}

//----------------------------------------------------------------------------------------------------------------
static bool HEX_Read_old()
{
	// ������ ( �� ����������� ) ������� ��� �������� HEX ����� � ���������� �������� � ������� ( ProgramblockofFlashProgramMemory ) 
	// ��� ����������� ������������ ������� � �� ������. ��� �� ����� ������������ ��������� ������ HEX �����
	
	HEX_File hex_file;
	char numbuf[ 2 ];
	char mark = 0;
	int max_data_byte_length = 0;
	uint32_t hex_file_byte_count = 0;
	int hex_file_data_byte_count = 0;	
	
	while( 1 )
	{
		SD_ReadFile( hex_file_byte_count, 1, &mark );
		if ( mark == ':' )
		{
			uint16_t mark_point = hex_file_byte_count;
			SD_ReadFile( mark_point + 1, 2, &numbuf );
			hex_file.recordLength = strtol( &numbuf, NULL, 16 );	
			SD_ReadFile( mark_point + 3, 2, &numbuf );
			hex_file.loadAdDressH = strtol( &numbuf, NULL, 16 );
			SD_ReadFile( mark_point + 5, 2, &numbuf );
			hex_file.loadAdDressL = strtol( &numbuf, NULL, 16 );
			SD_ReadFile( mark_point + 7, 2, &numbuf );
			hex_file.recordType = strtol( &numbuf, NULL, 16 );			
			if( hex_file.recordLength == 0x00 )
				if ( hex_file.recordType == END_RECORD )
					return true;
			hex_file_data_byte_count = 0;
			max_data_byte_length = ( hex_file.recordLength * 2 );			
			for( int j = mark_point; j < ( mark_point + max_data_byte_length ); j++ )
			{
				SD_ReadFile( j + 9, 2, &numbuf );
				hex_file.Data[ hex_file_data_byte_count ] = strtol( &numbuf, NULL, 16 );				
				hex_file_data_byte_count++;
				j++;
			}
			SD_ReadFile( mark_point + 9 + ( hex_file.recordLength * 2 ), 2, &numbuf );
			hex_file.Checksum = strtol( &numbuf, NULL, 16 );
			L_GREEN_RESET;
			if ( hex_file.Checksum == CRC_HEX( &hex_file ) )
			{
				if( ProgramblockofFlashProgramMemory( &hex_file ) == ResponseACK )
					L_GREEN_SET;
				else
				{
					L_RED_SET;
					SetString( 1, 9, "Err", 3 );
					_delay_ms( 3500 );
					return false;
				}					
			}					
			else
			{
				L_RED_SET;
				SetString( 1, 13, "CRC", 3 );
				_delay_ms( 3500 );
				return false;
			}				
			hex_file_byte_count = hex_file_byte_count + 13 + hex_file.recordLength * 2;
		}
	}
	return true;
}

//----------------------------------------------------------------------------------------------------------------
static void Draw_DIR_List( int8_t list_count, _path path[] )
{
	// ������� ������ ������ ������ � ����� �� �������
	
	int temp = list_count / 4;	
	for( int8_t i = 0; i <= 3; ++i)
		SetString(	i, 
					1, 
					&path[ temp * 4 + i ].f_inf.fname, 
					strlen( &path[ temp * 4 + i ].f_inf.fname ) );
}

//----------------------------------------------------------------------------------------------------------------
static void menu_update( bool direction )
{
	// ���������� ������� ��� ������� ������ ����� � ����
	
	_delay_ms(300);	// �������� ��� ���������� �������� ���������
	set_increment_numb( 0 );	
	set_max_increment_numb( 60 );		
	Clear_Display();	// ������� �������	
	_delay_ms(30);
	SetString( list_count, 0, " ", 1 );
	if( direction )
	{
		index_count--;
		if( index_count < 0 )
			index_count = path_index - 1;
	}			
	else
	{
		index_count++;
		if( index_count > path_index - 1 )
			index_count = 0;	
	}					
	list_count = arrow_index_calculate( index_count );	
	set_cursor( list_count );	
	Draw_DIR_List( index_count, &custom_path );
}

char temp_path[ 20 ];
char tp[ 20 ];
char slash[ 2 ] = "/\0";

//----------------------------------------------------------------------------------------------------------------
void SD_Work()
{
	// �������� ������ � �������
	
	memset( temp_path, 0, 20 );				// init temp_path
	set_max_increment_numb( 60 );			// ��������� ������� ���������� 60 ������
	Clear_Display();						// ������� �������
	SetString( 0, 0, "SD Mount... ", 12 );	// ���������� ����������
	if( SD_Mount() )						// ��������� ������
		SetString( 0, 12, "Ok", 2 );		// ���������� ����������
	else 
	{
		SetString( 0, 12, "Err", 3 );		// ���������� ����������
		_delay_ms( 1000 );
		return;
	}	
	root_dir_init();
	SetString( 1, 0, "Open DIR... ", 12 );		// ���������� ����������
	res = pf_opendir( &dir, &custom_path[ 1 ].f_inf.fname );	// ��������� �������� �����
	if( res == FR_OK )
		SetString( 1, 12, "Ok", 2 );			// ���������� ����������
    else 
	{
		SetString( 1, 12, "Err", 3 );			// ���������� ����������
		_delay_ms( 1000 );
		return;
	}		
	
	while( path_index != 31 )					// 31 ��� ������ ���������� �������� ������� custom_path[]
	{
		SetString( 2, 0, "Read DIR... ", 12 );	// ���������� ����������
		res = pf_readdir( &dir, &fileinfo );	// ������ ����������
		if( res != FR_OK )						
		{
			SetString( 2, 12, "Err", 3 );
			_delay_ms( 1000 );
			return;
		}			
		else									// ���� ������� �������
		{
			SetString( 2, 12, "Ok", 2 );
			
			if( fileinfo.fname[ 0 ] == 0 )		
				break;							// ���� ����� �� ������� �� �����
			else	
			{
				custom_path[ path_index ].f_inf = fileinfo;
				custom_path[ path_index ].HEX = false;		// ����� ��� ��� ��������� �� hex ����� ������ false	
				//SendStr( custom_path[ path_index ].f_inf.fname );
				//SendStr( "\n\r" );
				Check_for_HEX_procedure( path_index );		// ��� �� ���������� ����� �� ���� ��� ����� ���������� .hex
				path_index++;	
			}	
		}	
	}
	Clear_Display();						// ������� �������	
	_delay_ms( 30 );
	Draw_DIR_List( index_count, &custom_path );	
	SetString( list_count, 0, ">", 1 );
	while( 1 )
	{
		if( BUTTON_UP )						// ���� ������ ������ "UP"
			menu_update( DIRECTION_UP );
		if( BUTTON_DOWN )					// ���� ������ ������ "DOWN"
			menu_update( DIRECTION_DOWN );
		if( BUTTON_SELECT )
		{
			_delay_ms( 300 );
			set_increment_numb( 0 );
			
			L_RED_RESET;
			L_GREEN_RESET;
			
			if( index_count == 0 )	
				break;			
			if( custom_path[ index_count ].HEX )
			{
				set_max_increment_numb( 200 );
				Clear_Display();				
				_delay_ms( 100 ) ;
				Show_microcontroller_name_and_version();
				_delay_ms( 100 );
				// ����� ������������ �������� ����� �������� � ���������������� �����������
				if( temp_path[ 0 ] != 0 )
				{
					if( strncmp( tp, custom_path[ index_count ].f_inf.fname, sizeof( custom_path[ index_count ].f_inf.fname ) ) )
					{
						strcpy( tp, custom_path[ index_count ].f_inf.fname );
						strcat( temp_path, slash );
						strcat( temp_path, custom_path[ index_count ].f_inf.fname );
					}
					SD_OpenFile( &temp_path );	
				}
				else
					SD_OpenFile( &custom_path[ index_count ].f_inf.fname );	
				SetString( 0, 0, "Erase Flash: ", 13 );	// ���������� ����������	
				if(	EraseFlash_ProgramMemoryOnlyCommand() == ResponseACK )				{					SetString( 0, 12, "Ok", 2 );					SetString( 1, 0, "Firmware:", 9 );
					SetString( 1, 10, "...", 3 );					_delay_ms( 100 );#if 0									// ��������� ������� �������� ��������					if( HEX_Read_old() )		#else					// ����� ����� ������� ������� �������� ��������					if( HEX_Read( &custom_path[ index_count ].f_inf.fsize ) )	#endif						SetString( 1, 9, "Ok  ", 4 );					else						SetString( 1, 9, "Err ", 4 );					While_Select_Press();				}									else				{					SetString( 0, 12, "Err", 3 );					_delay_ms( 1500 );				}
			}
			else
			{
				res = pf_opendir( &dir, &custom_path[ index_count ].f_inf.fname );	// ��������� �����
				if( res == FR_OK )
				{
					if( index_count != 1 )
					{
						memset( temp_path, 0, 20 );
						strcpy( temp_path, custom_path[ index_count ].f_inf.fname );
					}						
					else
						memset( temp_path, 0, 20 );
					SendStr( &temp_path );
					root_dir_init();
					path_index = 2;
					while( path_index != 31 )		// 31 ��� ������ ���������� �������� ������� custom_path[]
					{
						res = pf_readdir( &dir, &fileinfo );	// ������ ����������
						if( res == FR_OK )						// ���� ������� �������
							if( fileinfo.fname[ 0 ] != 0 )		// � ���� �� �����
							{
								custom_path[ path_index ].f_inf = fileinfo;
								custom_path[ path_index ].HEX = false;		// ����� ��� ��� ��������� �� hex ����� ������ false	
								Check_for_HEX_procedure( path_index );		// ��� �� ���������� ����� �� ���� ��� ����� ���������� .hex
								path_index++;
							}				
							else	// ���� ����� �� ������� �� �����
								break;
						else 
							SetString( 0, 12, "Err", 3 );
					}
					Clear_Display();	// ������� �������	
					_delay_ms( 30 );
					list_count = 1;
					index_count = 1;
					Draw_DIR_List( index_count, &custom_path );
					SetString( list_count, 0, ">", 1 );
				}
				else 
					SetString( 0, 12, "Err", 3 );
			}
			Clear_Display();	// ������� �������	
			_delay_ms(30);	
			Draw_DIR_List( index_count, &custom_path );	
			set_cursor( list_count );		
		}	
	} // while( 1 )
	root_dir_deinit();
	SD_UNMount();
}

