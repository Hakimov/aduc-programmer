/*
 * modbus.h
 *
 * Created: 28.08.2017 15:58:17
 *  Author: hakimov_a
 */ 


#ifndef MODBUS_H_
#define MODBUS_H_

#define F_CPU	( 11059200UL )

#include <avr/io.h>
#include <avr/delay.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "UART.h"
#include "WHLCD.h"

#define MODBUS_DEVICE_ID0					( 0x77 ) // id ��������� ������
#define MODBUS_DEVICE_ID1					( 0xF7 ) // id ������������ ������

#define Read_Discrete_Inputs				( 0x02 ) //� ������ �������� �� ���������� ���������� ������ (Read Discrete Inputs).
#define Read_Holding_Registers				( 0x03 ) //� ������ �������� �� ���������� ��������� �������� (Read Holding Registers).
#define Read_Input_Registers				( 0x04 ) //� ������ �������� �� ���������� ��������� ����� (Read Input Registers).
#define Force_Single_Coil					( 0x05 ) //� ������ �������� ������ ����� (Force Single Coil).
#define Preset_Single_Register				( 0x06 ) //� ������ �������� � ���� ������� �������� (Preset Single Register).
#define Read_Exception_Status				( 0x07 ) //� ������ �������� ��������� (Read Exception Status)
#define Force_Multiple_Coils				( 0x0F ) //� ������ �������� � ��������� ��������� ������ (Force Multiple Coils)
#define Preset_Multiple_Registers			( 0x10 ) //� ������ �������� � ��������� ��������� �������� (Preset Multiple Registers)
#define Mask_Write_Register					( 0x16 ) //� ������ � ���� ������� �������� � �������������� ����� �Ȼ � ����� ���Ȼ (Mask Write Register).
#define Read_FIFO_Queue						( 0x18 ) //� ������ ������ �� ������� (Read FIFO Queue)
#define Read_File_Record					( 0x14 ) //� ������ �� ����� (Read File Record)
#define Write_File_Record					( 0x15 ) //� ������ � ���� (Write File Record)
#define Diagnostic							( 0x08 ) //� ����������� (Diagnostic)
#define Get_Com_Event_Counter				( 0x0B ) //� ������ �������� ������� (Get Com Event Counter)
#define Get_Com_Event_Log					( 0x0C ) //� ������ ������� ������� (Get Com Event Log)
#define Report_Slave_ID						( 0x11 ) //� ������ ���������� �� ���������� (Report Slave ID)
#define Encapsulated_Interface_Transport	( 0x2B ) //� Encapsulated Interface Transport

//-------------------------------------------------------------------------------------------------------
#define Function_code			( 0x2B ) // 1 Byte 
#define MEI_Type				( 0x0E ) // 1 byte 
#define Read_Device_ID_code		( 0x01 ) // 1 Byte  / 02 / 03 / 04
#define Conformity_level		( 0x01 ) // 1 Byte
#define More_Follows			( 0x00 ) // 1 Byte 00 / FF
#define Next_Object_Id			( 0x00 ) // 1 Byte Object ID number
// next parameter - Number_of_objects	 // 1 Byte

// List_Of: 
// 1. Object_ID				// 1 Byte
// 2. Object_length			// 1 Byte
// 3. Object_Value_Object	// length Depending on the object ID
//-------------------------------------------------------------------------------------------------------

/* ERROR CODES */
// ������������ ��������� ����� ������
enum {
	NoError = 0,
	DEV_ID_Err,
	FuncCode_Err,
	MEI_Type_Err,
	DevIDcode_Err,
	Conf_lvl_Err,
	MoreFllws_Err,
	NextObjId_Err,
} ERROR_CODES;

//-----------------------------------------------------------------------------
// ������� ��� ������� ������ ��������
void request_sofware_version( uint8_t DEVICE_ID );


#endif /* MODBUS_H_ */