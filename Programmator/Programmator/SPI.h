/*
 * SPI.h
 *
 * Created: 16.08.2017 8:04:48
 *  Author: hakimov_a
 */ 


#ifndef SPI_H_
#define SPI_H_

#define CHIP_SELECT_SET		( PORTB |= 0x01 )
#define CHIP_SELECT_RESET	( PORTB &= ~0x01 )

#include <avr/io.h>



void SPI_MasterInit( void );
unsigned char SPI_MasterTransmit( unsigned char cData );
uint8_t SPI_MasterReceive( void );


#endif /* SPI_H_ */