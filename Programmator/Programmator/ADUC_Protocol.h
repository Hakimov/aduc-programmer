/*
 * ADUC_Protocol.h
 *
 * Created: 16.08.2017 16:04:54
 *  Author: hakimov_a
 */ 


#ifndef ADUC_PROTOCOL_H_
#define ADUC_PROTOCOL_H_

#define F_CPU	11059200UL

#include <avr/io.h>
#include <avr/delay.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "UART.h"
#include "PORT.h"
#include "WHLCD.h"

// ������� ��� ������ � ������������� ADUC
// Erase Flash/EE Program Memory Only 
#define Command_C ( 0x43 ) // Response ACK (0x06) NAK (0x07)
// Erase Flash/EE Program and Data Memory
#define Command_A ( 0x41 ) // Response ACK (0x06) NAK (0x07)
// Program block of Flash/EE Program Memory
#define Command_W ( 0x57 ) // Response ACK (0x06) NAK (0x07)
// Read Page of Flash/EE Program Memory
#define Command_V ( 0x56 ) // 0x100 data bytes + CS NAK (0x07)
// Page Download of Flash/EE Program Memory
#define Command_Q ( 0x51 ) // Response ACK (0x06) NAK (0x07)
// Program block of Flash/EE Data Memory
#define Command_E ( 0x45 ) // Response ACK (0x06) NAK (0x07)
// Set Security Modes (All Parts Except ADuC812)
#define Command_S ( 0x53 ) // Response ACK (0x06) NAK (0x07)
// Change Baud Rate (Timer 3 Enabled Part Only)
#define Command_B ( 0x42 ) // Response ACK (0x06) NAK (0x07)
// Jump to User code (Remote RUN) Command
#define Command_U ( 0x55 ) // Response ACK (0x06) NAK (0x07)
// Set ETIM Registers (ADuC812 only with V2.22 Kernel or Later)
#define Command_T ( 0x54 ) // Response ACK (0x06) NAK (0x07)
// Enable Bootload Command (ULOAD)
#define Command_F ( 0x46 ) // Response ACK (0x06) NAK (0x07)

// ��� ������ ����������� ADUC �������� ������ �� � ���
#define ResponseACK ( 0x06) // �� - �������� ������� ���������
#define ResponseNAK ( 0x07)	// ��� - ��������� ������

// ��� �������� ������ ���������� �������� ������ � ������ ������� ������ ���� ��������� 
// ��������������:
#define PACKET_ID1	( 0x07 )
#define PACKET_ID2	( 0x0E )	

// ��� ��������� �������� ����� ���������� ����������� ADUC
typedef struct DataTransportPacket
{
	uint8_t packetStartID1;		// ID1
	uint8_t packetStartID2;		// ID2
	uint8_t NoOfDataBytes;		// ���������� ���� � ���� ������
	uint8_t DataCommandFunction;// �������
	uint8_t dataBytes[ 30 ];	// ������
	uint8_t checksum;			// ���������� �����
} dataTransportPacket;


// ���� ������ ����� �� �����
// static char ADUC_info[] = { 0x41, 0x44, 0x49, 0x20, 0x38, 0x34, 0x32, 0x20, 0x20, 0x20, 0x56, 0x32, 0x31, 0x37, 0x0D, 0x0A, 0x78, 0x88, 0xCC, 0x01, 0x07, 0x8F, 0x01, 0x00, 0xA9 };


// ��������� HEX �����
typedef struct HEX_FILE
{	
	char recordMark;		// ������ ������
	uint8_t recordLength;	// ����� 
	uint8_t loadAdDressH;	// ������� ���� ������
	uint8_t loadAdDressL;	// ������� ���� ������
	uint8_t recordType;		// ��� ������
	uint8_t Data[ 30 ];		// ������
	uint8_t Checksum;		// ����������� �����
} HEX_File;

// ��� ������� ��������� � ���� recordType
#define DATA_RECORD		( 0x00 )	// ������
#define END_RECORD		( 0x01 )	// �����


// ��������� �������
uint8_t EraseFlash_ProgramMemoryOnlyCommand();	// ������� ��������� �� ������ ������� ��� ���� ������
uint8_t ProgramblockofFlashProgramMemory( HEX_File* );	// ������� ��������
bool Request_of_microcontroller_version( unsigned char *_str );	// ������ ���� � ������ ����������������
uint32_t ADUC_ChangeBAUDRATE( uint32_t baud );


#endif /* ADUC_PROTOCOL_H_ */