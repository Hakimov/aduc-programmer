/*
 * CProgram1.c
 *
 * Created: 16.08.2017 16:04:17
 *  Author: hakimov_a
 */ 

#include "ADUC_Protocol.h"
#include <math.h>

const unsigned char str2[ 5 ] = "ADI \0";

//----------------------------------------------------------------------
static uint8_t ChecksumCalculate( uint8_t NoOfDataBytes, uint8_t *Data ) 
{
	// ������� ��� ������� ����������� �����
	
	uint8_t dataSum = 0;	
	for ( int i = 0; i < NoOfDataBytes; ++i )
		dataSum += Data[ i ];
	uint8_t result = 0x0100 - ( NoOfDataBytes + dataSum );	
	return result;
}

//----------------------------------------------------------------------
uint32_t ADUC_ChangeBAUDRATE( uint32_t baud )
{
	// ������� ��� ��������� �������� �������� UART � ADUC
	
	uint32_t	Fcore			= 2097152*8;
	double		DIV				= ( log( Fcore / ( 16 * baud ) ) / log( 2 ) ) - 1;
	double		T3FD			= ceil( ( double ) ( 2 * Fcore ) / ( pow( 2, ( uint32_t ) DIV ) * baud ) - 64 );
	double		ActualBaudRate	= ( 2 * Fcore ) / ( pow ( 2, ( uint32_t ) DIV ) * ( T3FD + 64 ) );
	uint32_t	CRC				= 0x100 - ( 0x03 + 0x42 + (0x80 + ( uint32_t ) DIV ) + T3FD );
	// Send packet
	USART_Transmit( PACKET_ID1 );
	USART_Transmit( PACKET_ID2 );
	USART_Transmit( 0x03 );			// NoOfDataBytes
	USART_Transmit( Command_B );
	USART_Transmit( 0x80 + ( int8_t ) DIV );
	USART_Transmit( T3FD );
	USART_Transmit( CRC );	
	while( USART_Receive() != ResponseACK );
	return ( uint32_t ) ActualBaudRate;
}

//----------------------------------------------------------------------
uint8_t EraseFlash_ProgramMemoryOnlyCommand()
{	
	// ������� ������� ������ ����������� ADUC � ����������� ������
	
	// ������� ������ ������(������) � ��������� tempDTP
	dataTransportPacket		tempDTP;
	tempDTP.packetStartID1	= PACKET_ID1;
	tempDTP.packetStartID2	= PACKET_ID2;
	tempDTP.NoOfDataBytes	= 0x01;
	tempDTP.dataBytes[ 0 ]	= Command_C;
	tempDTP.checksum		= ChecksumCalculate( tempDTP.NoOfDataBytes, &tempDTP.dataBytes );
	
	// �������� ������
	USART_Transmit( tempDTP.packetStartID1 );
	USART_Transmit( tempDTP.packetStartID2 );
	USART_Transmit( tempDTP.NoOfDataBytes );
	USART_Transmit( tempDTP.dataBytes[ 0 ] );
	USART_Transmit( tempDTP.checksum );

	// ����� ������� ����� � ������� ���������
	uint32_t lcount = 0;
	while( USART_Receive() != ResponseACK )
	{
		++lcount;
		if( lcount > 0x0FFE )
 			return 0xFF;			
	}
	return ResponseACK;	
}

//----------------------------------------------------------------------
uint8_t ProgramblockofFlashProgramMemory( HEX_File* hex_file )
{
	// ������� ��� ���������� �������� HEX �����
	
	dataTransportPacket		tempDTP;			// ��������� ��� ������������ ������
	tempDTP.packetStartID1	= PACKET_ID1;		// ����� ���� ��� ����������
	tempDTP.packetStartID2	= PACKET_ID2;
	tempDTP.NoOfDataBytes	= hex_file->recordLength + 4;
	tempDTP.dataBytes[ 0 ]	= Command_W;	
	tempDTP.dataBytes[ 1 ]	= 0x00;
	tempDTP.dataBytes[ 2 ]	= hex_file->loadAdDressH;
	tempDTP.dataBytes[ 3 ]	= hex_file->loadAdDressL;
	
	for( int i = 4; i < ( hex_file->recordLength + 4 ); ++i )
		tempDTP.dataBytes[ i ] = hex_file->Data[ i - 4 ]; 

	tempDTP.checksum = ChecksumCalculate( tempDTP.NoOfDataBytes, tempDTP.dataBytes );
	
	USART_Transmit( tempDTP.packetStartID1 );	// � ��������
	USART_Transmit( tempDTP.packetStartID2 );
	USART_Transmit( tempDTP.NoOfDataBytes );
	for( int i = 0; i < ( tempDTP.NoOfDataBytes ); ++i )
		USART_Transmit( tempDTP.dataBytes[ i ] );
	USART_Transmit( tempDTP.checksum );
	
	uint32_t lcount = 0;	
	while( USART_Receive() != 0x06 )			// ����� ������� ����� � ������� ���������
	{
		++lcount;
		if( lcount > 0x0FFE )	// ����� ������� ��� ���� ��� �� �� �������� � ����� ��� �������� ������
 			return 0xFF;			
	}
	return ResponseACK;	
}

//----------------------------------------------------------------------
bool Request_of_microcontroller_version( unsigned char _str[] )
{	
	// ������ ���� � ������ ����������������
	//$21$5A$00$A6 - ������
	//$41$44$49$20$38$34$32$20$20$20$56$32$31$37$0D$0A$78$88$CC$01$07$8F$01$00$A9 - ��������� �����	
	
	static char name[ 5 ];
	static char number[ 5 ];
	static char version[ 5 ];
	
	memset( name, 0, 5 );
	memset( number, 0, 5 );
	memset( version, 0, 5 );

#if 0	
	USART_Transmit( 0xFF );
	_delay_ms( 10 );
#endif

	USART_Transmit( 0x21 );
	USART_Transmit( 0x5A );
	USART_Transmit( 0x00 );
	USART_Transmit( 0xA6 );
		
	for( int yy = 0; yy <= 3; ++yy )
		name[ yy ] = USART_Receive();
	
	if( strncmp( name, str2, 3 ) )
		return false;		
	
	for( int yy = 0; yy <= 3; ++yy )
		number[ yy ] = USART_Receive();
	
	for( int yy = 0; yy <= 1; ++yy )
		USART_Receive();
	
	for( int yy = 0; yy <= 3; ++yy )
		version[ yy ] = USART_Receive();
				
#if 0
	for( int yy = 0; yy <= 10; ++yy )
		USART_Receive();	
#endif
	
	strncpy(	_str, 
				name, 
				strlen( &name ) );
				
	strncpy(	_str + strlen( name ), 
				&number, 
				strlen( &number ) );
				
	strncpy(	_str + strlen( name ) + strlen( &number ), 
				&version, 
				strlen( &version ) );

	memset( name, 0, 5 );
	memset( number, 0, 5 );
	memset( version, 0, 5 );
	
	return true;
}


