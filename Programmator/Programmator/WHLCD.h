/*
 * WHLCD.h
 *
 * Created: 23.08.2017 11:49:59
 *  Author: hakimov_a
 */ 


#ifndef WHLCD_H_
#define WHLCD_H_

/********************************************************************************/
// DISPLAY WH1604

/* PIN FUNCTION
1  VSS	0V			Ground 
2  VDD	5V			Supply Voltage for logic 
3  VO	(Variable)  Operating voltage for LCD 
4  RS	H/L			H: DATA, L: Instruction code 
5  R/W	H/L			H: Read(MPU?Module) L: Write(MPU?Module) 
6  E	H,H?L		Chip enable signal 
7  DB0  H/L			Data bus line 
8  DB1  H/L			Data bus line 
9  DB2  H/L			Data bus line 
10 DB3  H/L			Data bus line 
11 DB4  H/L			Data bus line 
12 DB5  H/L			Data bus line 
13 DB6  H/L			Data bus line 
14 DB7  H/L			Data bus line 
15 A	-			LED + 
16 K	-			LED	- 
*/

/* Display position DDRAM address  4-Line by 16-Character Display 
	00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F 
	40 41 42 43 44 45 46 47 48 49 4A 4B 4C 4D 4E 4F 
	10 11 12 13 14 15 16 17 18 19 1A 1B 1C 1D 1E 1F 
	50 51 52 53 54 55 56 57 58 59 5A 5B 5C 5D 5E 5F
*/
#define F_CPU	11059200UL

// ���� � �������� ���������� ���� ������ ���
#define PORT_DATA	( PORTC )
#define PIN_DATA	( PINC )
#define DDRX_DATA	( DDRC )

// ���� � �������� ���������� ����������� ������
#define PORT_SIG	( PORTA )
#define PIN_SIG		( PINA )
#define DDRX_SIG	( DDRA )

// ������ ������� ���������������� � ������� ���������� ����������� ������ ���
#define RS			( 5 )
#define RW			( 6 )
#define EN			( 7 )

// ������� ��� ������ � ������
#define ClearBit( reg, bit )	reg &= ( ~( 1 << ( bit ) ) )
#define SetBit( reg, bit )		reg |= ( 1 << ( bit ) )

#include <avr/io.h>
#include <util/delay.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// ������� ��� ������� ������������ ������(�������, ������� �� ����� ���� ������ ������� �������)
#define Clear_line( line )	SetString( line, 0, "                ", 16 );

// ������� ������ �������
void LcdWriteCommand( unsigned char );
 
// ������� ������ ������
void LcdWriteData( unsigned char );

// ������� �������������
void LCD_Init();
void start_menu();
void set_cursor( int8_t cursor_position );
void SetString( uint8_t line, uint8_t addr, unsigned char *str, uint8_t size );

void Clear_Display();
/* Write "20H" to DDRAM. and set DDRAM address to "00H" from AC. 1.52ms */
// RS  R/W  DB7  DB6  DB5  DB4 DB3 DB2 DB1 DB0
// 0	0	0	 0	  0		0	0	0	0	1

void Return_Home();
/* Set DDRAM address to �00H� from AC and return cursor to its original position if shifted. The contents of DDRAM are not changed. 1.52ms */
// RS  R/W  DB7  DB6  DB5  DB4 DB3 DB2 DB1 DB0
// 0	0	0	 0	  0		0	0	0	1	-

void Entry_Mode_Set();
/* Sets cursor move direction and specifies display shift.
These operations are performed during data write and read. 37?s */
// RS  R/W  DB7  DB6  DB5  DB4 DB3 DB2 DB1 DB0
// 0	0	0	  0	   0	0	0	1  I/D SH

void Display_ON_OFF_Control();
/*
D=1:entire display on
C=1:cursor on
B=1:cursor position on
37?s */
// RS  R/W  DB7  DB6  DB5  DB4 DB3 DB2 DB1 DB0
// 0	0	0	 0	  0		0	1	D	C	B

void Cursor_or_Display_Shift();
/* Set cursor moving and display shift control bit, and the direction, without changing DDRAM data. 37?s */
// RS  R/W  DB7  DB6  DB5  DB4 DB3 DB2 DB1 DB0
// 0	0	 0	  0		0	1  S/C R/L  -   -

void Function_Set();
/* 
DL:interface data is 8/4 bits. 
N:number of line is 2/1. 
F:font size is 5x11/5x8. 37?s */
// RS  R/W  DB7  DB6  DB5  DB4 DB3 DB2 DB1 DB0
// 0	0	0	  0	   1	DL	N	F	-	-

void Set_CGRAM_Address();
/* Set CGRAM address in address counter. 37?s */
// RS  R/W  DB7  DB6  DB5  DB4 DB3 DB2 DB1 DB0
// 0	0	0	 1	  AC5  AC4 AC3 AC2 AC1 AC0

void Set_DDRAM_Address( uint8_t );
/* Set DDRAM address in address counter. 37?s */
// RS  R/W  DB7  DB6  DB5  DB4 DB3 DB2 DB1 DB0
// 0	0	1	 AC6  AC5  AC4 AC3 AC2 AC1 AC0


void Read_Busy_Flag_and_Address();
/* Whether during internal operation or not can be known by reading BF. The contents of address counter can also be read. 0?s */
// RS  R/W  DB7  DB6  DB5  DB4 DB3 DB2 DB1 DB0
// 0	1	BF	 AC6  AC5  AC4 AC3 AC2 AC1 AC0

void Write_Data_to_RAM();
/* Write data into internal RAM (DDRAM/CGRAM). 37?s */
// RS  R/W  DB7  DB6  DB5  DB4 DB3 DB2 DB1 DB0
// 1	0	D7	 D6	  D5	D4 D3	D2 D1	D0

void Read_Data_from_RAM();
/* Read data from internal RAM (DDRAM/CGRAM) 37?s */
// RS  R/W  DB7  DB6  DB5  DB4 DB3 DB2 DB1 DB0
// 1	1	D7	 D6	  D5	D4  D3  D2  D1  D0




#endif /* WHLCD_H_ */