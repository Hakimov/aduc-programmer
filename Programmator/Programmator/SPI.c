/*
 * SPI.c
 *
 * Created: 16.08.2017 8:04:58
 *  Author: hakimov_a
 */ 

#include "SPI.h"

/********************************************************************************/
// SPI

//-----------------------------------------------------------------------------
void SPI_MasterInit( void )
{
	// �������������
	
	DDRB	= 0xF7; //( 1 << DDRB2 ) | ( 1 << DDRB1 ) | ( 0 << DDRB0 );
	PORTB	= 0x00;
	CHIP_SELECT_SET;
	/* ���������� SPI � ������ �������, ��������� �������� ����� fck/16 */
	//SPCR = ( 1 << SPE ) | ( 1 << MSTR ) | ( 0 << SPR0 );
	SPCR	= 0x50;
	SPSR	= 0x01;
}

//-----------------------------------------------------------------------------
unsigned char SPI_MasterTransmit( unsigned char cData )
{
	// �������� �����
	
	CHIP_SELECT_RESET;	
	SPDR = cData;	/* ������ �������� ������ */
	while( !( SPSR & ( 1 << 7 ) ) )
		cData = SPDR;		
	CHIP_SELECT_SET;
	return cData;
}

//-----------------------------------------------------------------------------
uint8_t SPI_MasterReceive( void )
{
	// ����� �����
	
	uint8_t temp = 0;
	temp = SPDR;
}
