/*
 * GccApplication1.c
 *
 * Created: 15.08.2017 9:57:05
 *  Author: hakimov_a
 */ 

/* 
Files name:
	"842.HEX"
	"���_V4.1v.HEX"
*/

#include "Programmator.h"

	
/********************************************************************************/
// CUSTOM

/*
*  for use printf using UART
*
*  sprintf( &str, "Hello\n\r");
*  SendStr( &str );
*
*/

/********************************************************************************/
// MAIN
int main( void )
{
	int8_t cursor_position = 1;	
	init_port();	
	USART_Init( 9600 );	SPI_MasterInit();
	ADC_Init();
	
	// ����� �������� ����� ����������
	Timer_Init( 1, 15 );	// ��������� ������������ ������ �������(�������� 6) � ������������ ����� ���������� 15
	LCD_Init();	
	sei();
	
	L_GREEN_RESET;
	L_RED_RESET;
	
	start_menu();
	set_cursor( cursor_position );
	while( 1 )
	{
		ADC_Voltage_Percent_print();
		_delay_ms( 50 );
		if( BUTTON_UP )
		{
			_delay_ms( 300 );
			set_increment_numb( 0 );
			SetString( cursor_position, 0, " ", 1 );
			cursor_position--;
			if( cursor_position < 1 )
				cursor_position = 2;
			set_cursor( cursor_position );
		}
		if( BUTTON_DOWN )
		{
			_delay_ms( 300 );
			set_increment_numb( 0 );
			SetString( cursor_position, 0, " ", 1 );
			cursor_position++;
			if( cursor_position > 2 )
				cursor_position = 1;
			set_cursor( cursor_position );						
		}
		if( BUTTON_SELECT )
		{
			_delay_ms( 300 );
			set_increment_numb( 0 );
			switch( cursor_position )
			{
				case 1:					
					SD_Work();
					start_menu();
					set_cursor( cursor_position );	
					break;
				case 2:					
					Clear_Display();
					_delay_ms( 100 );
					request_sofware_version( MODBUS_DEVICE_ID1 );
					request_sofware_version( MODBUS_DEVICE_ID0 );					
					While_Select_Press();
					start_menu();
					set_cursor( cursor_position );
					break;
				default:
					break;
			}		
		}
	} // while( 1 )
}
