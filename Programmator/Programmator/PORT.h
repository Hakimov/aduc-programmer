/*
 * PORT.h
 *
 * Created: 24.08.2017 16:31:21
 *  Author: hakimov_a
 */ 


#ifndef PORT_H_
#define PORT_H_

// ������� ���������� ����������
#define F_CPU	11059200UL

#include <avr/io.h>
#include <avr/delay.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// ���������/���������� �������� ����������
#define L_RED_SET		( PORTA |= 0b00000100 )
#define L_RED_RESET		( PORTA &= ~0b00000100 )

// ���������/���������� �������� ����������
#define L_GREEN_SET		( PORTA |= 0b00001000 )
#define L_GREEN_RESET	( PORTA &= ~0b00001000 )

// ���������/���������� �������������, ���� ����� ��������� �� ����� �������������� ����,
// ��� ��������� ������� ������ �� ���� ������ ���� ����������� �������� ��� ���� ������� �� ���������
#define DEVICE_ON		( PORTA |= 0b00010000 )
#define DEVICE_OFF		( PORTA &= ~0b00010000 )

// ��� ������� ������������� ��� ����������� ������
#define BUTTON_UP		( PIND & ( 1 << PD2 ) )	// �����
#define BUTTON_DOWN		( PIND & ( 1 << PD1 ) ) // ����
#define BUTTON_SELECT	( PIND & ( 1 << PD0 ) ) // �����

void init_port();
void While_Select_Press();


#endif /* PORT_H_ */