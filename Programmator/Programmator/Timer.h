/*
 * Timer.h
 *
 * Created: 31.08.2017 11:03:13
 *  Author: hakimov_a
 */ 


#ifndef TIMER_H_
#define TIMER_H_

#define F_CPU	11059200UL

#include <avr/io.h>
#include <avr/delay.h>
#include <avr/iom128a.h>
#include <avr/interrupt.h>

#include "WHLCD.h"
#include "PORT.h"


void Timer_Init( uint16_t sec, uint32_t max_incr_numb );	
void set_increment_numb( uint32_t numb );		// ��������� ����� ���������� ������� ���������������� � ���������� �������
uint32_t get_increment_numb();					// ��������� ����� �����


#endif /* TIMER_H_ */