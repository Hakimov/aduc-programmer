/*
 * WHLCD.c
 *
 * Created: 23.08.2017 11:49:48
 *  Author: hakimov_a
 */ 

#include "WHLCD.h"

// ������� ��� ������ ���������
const char letters[ 111 ] = 
"�������������������"
"�������������1234567"
"890�����Ũ����������"
"����������������.-;:"
", +?DFGIJLNQRSUVWXYZ"
"ABCEKMHOPT/";

const char letter_codes[ 111 ] = { 
0x61,0xB2,0xB3,0xB4,0xE3,0x65,0xB5,0xB6,0xB7,0xB8,
0xB9,0xBA,0xBB,0xBC,0xBD,0x6F,0xBE,0x70,0x63,0xBF,
0x79,0xE4,0x78,0xE5,0xC0,0xC1,0xE6,0xC2,0xC3,0xC4,
0xC5,0xC6,0xC7,0x31,0x32,0x33,0x34,0x35,0x36,0x37,
0x38,0x39,0x30,0x41,0xA0,0x42,0xA1,0xE0,0x45,0xA2,
0xA3,0xA4,0xA5,0xA6,0x4B,0xA7,0x4D,0x48,0x4F,0xA8,
0x50,0x43,0x54,0xA9,0xAA,0x58,0xE1,0xAB,0xAC,0xE2,
0xAD,0xAE,0x62,0xAF,0xB0,0xB1,0x2E,0x2D,0x3B,0x3A,
0x2C,0x10,0x2B,0x3F,0x44,0x46,0x47,0x49,0x4A,0x4C,
0x4E,0x51,0x52,0x53,0x55,0x56,0x57,0x58,0x59,0x5A,
0x41,0x42,0x43,0x45,0x4B,0x4D,0x48,0x4F,0x50,0x54,
0x2F
};


//------------------------------------------------------------------------------
uint8_t bit_reverse( uint8_t input_byte )
{
	// ��� ������� �������� �������� ������ ��� ������ ������� ���� �������� �������
	// ������ ����������� ������� ( 0-0 1-1 ... 7-7 ) ���� ������������ ( 0-7 1-6 ... 7-0 )
	// ������� ������ ������ ����� � �����
	
	uint8_t temp_byte = 0;	
	temp_byte |= ( input_byte & 0x01 ) << 7;
	temp_byte |= ( input_byte & 0x02 ) << 5;
	temp_byte |= ( input_byte & 0x04 ) << 3;
	temp_byte |= ( input_byte & 0x08 ) << 1;
	temp_byte |= ( input_byte & 0x10 ) >> 1;
	temp_byte |= ( input_byte & 0x20 ) >> 3;
	temp_byte |= ( input_byte & 0x40 ) >> 5;
	temp_byte |= ( input_byte & 0x80 ) >> 7;	
	return temp_byte;
}

//------------------------------------------------------------------------------
void LcdWriteCommand( unsigned char data )
{
	// ������� ������ �������
	
	ClearBit( PORT_SIG, RS );			// ������������� RS � 0
	PORT_DATA = bit_reverse( data );	// ������� ������ �� ����    
	SetBit( PORT_SIG, EN );				// ������������� � � 1
	_delay_us( 2 );
	ClearBit( PORT_SIG, EN );			// ������������� � � 0
	_delay_us( 40 );
}

//------------------------------------------------------------------------------
void LcdWriteData( unsigned char data )
{
	// ������� ������ ������
	
	SetBit( PORT_SIG, RS );				// ������������� RS � 1
	PORT_DATA = bit_reverse( data );	// ������� ������ �� ����     
	SetBit( PORT_SIG, EN );				// ������������� � � 1
	_delay_us( 2 );
	ClearBit( PORT_SIG, EN );			// ������������� � � 0
	_delay_us( 40 );
}
//------------------------------------------------------------------------------
void LCD_Init( void )
{
	// ������� ������������� ������� WH1604A
	
	//����������� ����� �����/������
	DDRX_DATA	= 0xFF;
	PORT_DATA	= 0xFF;
  
	DDRX_SIG	= 0xFF;
	PORT_SIG	|= ( 1 << RW ) | ( 1 << RS ) | ( 1 << EN );
	ClearBit( PORT_SIG, RW );
	_delay_ms( 40 );
  
	LcdWriteCommand( 0x38 );	//0b00111000 - 8 ��������� ����, 2 ������
	LcdWriteCommand( 0x0C );	//0b00001100 - �������, ������, �������� ��������
	LcdWriteCommand( 0x01 );	//0b00000001 - ������� �������
	_delay_ms( 2 );
	LcdWriteCommand( 0x06 );	//0b00000110 - ������ �������� ������, ������ ���
}


//------------------------------------------------------------------------------
void SetString( uint8_t line, uint8_t addr, unsigned char *str, uint8_t size )
{
	// ������� ������ ������ �� ������������� ������
	// uint8_t line - ������
	// uint8_t addr - �������
	// unsigned char *str - ��������� �� �����
	// uint8_t size - ����� ������
	
	
	int i = 0;
	int str_size = size;	
	if( str_size > 16 )
		str_size = 16;
		
	if( addr >= 0 || addr <= 15 )
	{
		if ( line == 0 )
			Set_DDRAM_Address( 0x00 | addr );
		else if ( line == 1 )
			Set_DDRAM_Address( 0x40 | addr );
		else if ( line == 2 )
			Set_DDRAM_Address( 0x10 | addr );
		else if ( line == 3 )
			Set_DDRAM_Address( 0x50 | addr );
	}		
		
	if( str_size >= ( 16 - addr ) )
		str_size = 16 - addr;	
				
	for( int i = 0; i < strlen( str ); i++ )
		for( int j = 0 ; j < 111; j++ )
			if( str[ i ] == letters[ j ] )
				str[ i ] = letter_codes[ j ];
				
	while( i != str_size )
	{
		LcdWriteData( str[ i ] );
		i++;		
	}
}

//------------------------------------------------------------------------------
void start_menu()
{	
	// ����� ���������� ���� ��� ���������
	
	Clear_Display();
	_delay_ms( 100 );
	SetString( 1, 1, "1.Open Flash", 12 );
	SetString( 2, 1, "2.Check Program", 15 );
	SetString( 3, 1, "  version", 9 );
}

//------------------------------------------------------------------------------
void set_cursor( int8_t cursor_position )
{
	// ��������� ��������� �� ������ ������ � ����
	
	SetString( cursor_position, 0, ">", 1 );
}


//------------------------------------------------------------------------------
void Clear_Display()
{	
	// ������� ������� �������

	/*
	Write "20H" to DDRAM. and set DDRAM address to "00H" from AC. 1.52ms
	RS  R/W  DB7  DB6  DB5  DB4 DB3 DB2 DB1 DB0
	0	0	0	 0	  0		0	0	0	0	1
	*/
	ClearBit( PORT_SIG, RS );			// ������������� RS � 0
	PORT_DATA = bit_reverse( 0x01 );	// ������� ������ �� ����    
	SetBit( PORT_SIG, EN );				// ������������� � � 1
	_delay_us( 2 );
	ClearBit( PORT_SIG, EN );			// ������������� � � 0
	_delay_us( 40 );
}

// ���� ���� ������� ������� ����� ����������� �� ���� ����������
//------------------------------------------------------------------------------
void Return_Home()
{	/*
	Set DDRAM address to �00H� from AC and return cursor to its original
	position if shifted. The contents of DDRAM are not changed. 1.52ms
	RS  R/W  DB7  DB6  DB5  DB4 DB3 DB2 DB1 DB0
	 0	0	0	 0	   0	0	0	0	1	-
	*/
}	

//------------------------------------------------------------------------------
void Entry_Mode_Set()
{	/*
	Sets cursor move direction and specifies display shift.
	These operations are performed during data write and read. 37s
	RS  R/W  DB7  DB6  DB5  DB4 DB3 DB2 DB1 DB0
	 0	0	0	  0	   0	0	0	1   I/D SH
	*/
}

//------------------------------------------------------------------------------
void Display_ON_OFF_Control()
{	/*
	D=1:entire display on
	C=1:cursor on
	B=1:cursor position on
	37s
	RS  R/W  DB7  DB6  DB5  DB4 DB3 DB2 DB1 DB0
	0	0	0	 0	  0		0	1	D	C	B
	*/
}

//------------------------------------------------------------------------------
void Cursor_or_Display_Shift()
{	/*
	Set cursor moving and display shift control bit, and the direction, without changing DDRAM data. 37s
	*/
	// RS  R/W  DB7  DB6  DB5  DB4 DB3 DB2 DB1 DB0
	// 0	0	 0	  0		0	1  S/C R/L  -   -
}

//------------------------------------------------------------------------------
void Function_Set()
{	/*
	DL:interface data is 8/4 bits 
	N:number of line is 2/1 
	F:font size is 5x11/5x8
	37?s
	*/
	// RS  R/W  DB7  DB6  DB5  DB4 DB3 DB2 DB1 DB0
	// 0	0	0	  0	   1	DL	N	F	-	-
}

//------------------------------------------------------------------------------
void Set_CGRAM_Address()
{	/*
	Set CGRAM address in address counter 37s
	*/
	// RS  R/W  DB7  DB6  DB5  DB4 DB3 DB2 DB1 DB0
	// 0	0	0	 1	  AC5  AC4 AC3 AC2 AC1 AC0
}

//------------------------------------------------------------------------------
void Set_DDRAM_Address( uint8_t addr )
{	/*
	Set DDRAM address in address counter 37s
	*/
	// RS  R/W  DB7  DB6  DB5  DB4 DB3 DB2 DB1 DB0
	// 0	0	1	 AC6  AC5  AC4 AC3 AC2 AC1 AC0
	ClearBit( PORT_SIG, RS );			// ������������� RS � 0
	PORT_DATA = bit_reverse( ( addr | 0x80 ) );	// ������� ������ �� ����    
	SetBit( PORT_SIG, EN );				// ������������� � � 1
	_delay_us( 2 );
	ClearBit( PORT_SIG, EN );			// ������������� � � 0
	_delay_us( 40 );
}

//------------------------------------------------------------------------------
void Read_Busy_Flag_and_Address()
{	/*
	Whether during internal operation or not can be known by
	reading BF. The contents of address counter can also be read. 0s
	*/
	// RS  R/W  DB7  DB6  DB5  DB4 DB3 DB2 DB1 DB0
	// 0	1	BF	 AC6  AC5  AC4 AC3 AC2 AC1 AC0
}

//------------------------------------------------------------------------------
void Write_Data_to_RAM()
{	/*
	Write data into internal RAM (DDRAM/CGRAM) 37s
	*/
	// RS  R/W  DB7  DB6  DB5  DB4 DB3 DB2 DB1 DB0
	// 1	0	D7	 D6	  D5	D4 D3	D2 D1	D0
}

//------------------------------------------------------------------------------
void Read_Data_from_RAM()
{	/*
	Read data from internal RAM (DDRAM/CGRAM) 37s
	*/
	// RS  R/W  DB7  DB6  DB5  DB4 DB3 DB2 DB1 DB0
	// 1	1	D7	 D6	  D5	D4  D3  D2  D1  D0
}

