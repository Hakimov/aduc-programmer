/*
 * SD_Working.h
 *
 * Created: 24.08.2017 13:11:08
 *  Author: hakimov_a
 */ 


#ifndef SD_WORKING_H_
#define SD_WORKING_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

// ��� ������ � ������� ���������� ��������� ����������
#include "pff.h"
#include "diskio.h"
#include "integer.h"

#include "PORT.h"
#include "UART.h"
#include "WHLCD.h"
#include "ADUC_Protocol.h"
#include "Timer.h"


#define DIRECTION_DOWN	false
#define DIRECTION_UP	true

char *str;

void SD_Work();

#endif /* SD_WORKING_H_ */