/*
 * PORT.c
 *
 * Created: 24.08.2017 16:31:03
 *  Author: hakimov_a
 */ 


#include "PORT.h"

/********************************************************************************/
void init_port()
{	
	// 0 - �� ����
	// 1 - �� �����
	
	DDRA = 0xFF;
	PORTA = 0xFF;
	
	DEVICE_ON;	// �� ��4 ������ ���� ������� �������, ��� ������ ������ ���������� �����������
	
	DDRC = 0xFF;
	PORTC = 0x00;
	
	DDRE = 0x02;	
	PORTE = 0x00;
	
	DDRF = 0x00;	
	PORTF = 0x00;
	
	PORTD = 0x80;	// ��� �������� ������ ��������� ��������� ������ �� �������� �� RS-485	
	DDRD = 0x80;	// PD7 ���������� �� ������ RE DE ���������� �������������� UART - RS-485
					// ��������� ������ ����� PD ��������� �� ����, 
					// PD0, PD1, PD2 - � ��� ������������ ������ 
}

void While_Select_Press()
{
	while( 1 )
	{
		if( BUTTON_SELECT )
		{
			_delay_ms( 300 );
			return;
		}
	}
}