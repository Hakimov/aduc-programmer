/*
 * Programmator.h
 *
 * Created: 16.08.2017 8:47:08
 *  Author: hakimov_a
 */ 


#ifndef PROGRAMMATOR_H_
#define PROGRAMMATOR_H_

#define F_CPU	11059200UL

#include "PORT.h"
#include "ADC.h"
#include "SPI.h"
#include "UART.h"
#include "WHLCD.h"
#include "Timer.h"
#include "ADUC_Protocol.h"
#include "SD_Working.h"
#include "modbus.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "pff.h"
#include "diskio.h"
#include "integer.h"

// char *str;

void HEX_Read();

#endif /* PROGRAMMATOR_H_ */